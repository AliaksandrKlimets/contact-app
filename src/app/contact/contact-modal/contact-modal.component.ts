import { ContactPerson } from './../../core/models/contact-person.model';
import { ContactInterService } from './../contact-inter.service';
import { MatDialog, MatRadioChange, MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CounterpartyType } from './../../core/models/counter-party-type.model';
import { Component, OnInit, OnChanges, ChangeDetectorRef, Inject } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ContactService } from 'src/app/core';
import { Address } from '../contact.component';

@Component({
  selector: 'app-contact-modal',
  templateUrl: './contact-modal.component.html',
  styleUrls: ['./contact-modal.component.css']
})
export class ContactModalComponent implements OnInit {

  private contactForm: FormGroup;
  private privatePersonForm: FormGroup;
  private organizationForm: FormGroup;

  private contactPerson: ContactPerson;

  constructor(
    private contactInterService: ContactInterService,
    private contactService: ContactService,
    private ref: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private dialogRef: MatDialogRef<ContactModalComponent>
  ) { }

  ngOnInit() {

    // all fields in according with api payloads

    this.privatePersonForm = new FormGroup({
      'CounterpartyType': new FormControl(CounterpartyType.PrivatePerson),
      'FirstName': new FormControl('', [Validators.required]),
      'MiddleName': new FormControl('', [Validators.required]),
      'LastName': new FormControl('', [Validators.required]),
      'Email': new FormControl(''),
      'Description': new FormControl('', [Validators.required]),
      'Phone': new FormControl('', [
        Validators.required,
        Validators.pattern('[\\d]{12}')]), //[+]{1}
      "CounterpartyProperty": new FormControl("Recipient"),
      "CounterpartyRef": new FormControl(),
      "Ref": new FormControl()
    })

    this.organizationForm = new FormGroup({
      'CounterpartyType': new FormControl(CounterpartyType.Organization),
      'FirstName': new FormControl('', [Validators.required]),
      'MiddleName': new FormControl('', [Validators.required]),
      'LastName': new FormControl('', [Validators.required]),
      'Email': new FormControl(''),
      'Description': new FormControl('', [Validators.required]),
      'Phone': new FormControl('', [
        Validators.required,
        Validators.pattern('[\\d]{12}')]), //[+]{1}
      'EDRPOU': new FormControl('', [Validators.required]), 
      'OwnershipFormDescription': new FormControl('', [Validators.required]),
      "CounterpartyProperty": new FormControl("Recipient")
    })

    this.contactForm = this.privatePersonForm;

    if (this.dialogData) {
      this.contactPerson = this.dialogData.contactPerson;
      this.privatePersonForm.setValue(this.contactPerson);
    }

  }

  radioChange($event: MatRadioChange) {
    
    switch ($event.value) {
      case CounterpartyType.PrivatePerson: 
        this.contactForm = this.privatePersonForm;
        break;
      case CounterpartyType.Organization: 
        this.contactForm = this.organizationForm;
        break;
      default:  
        this.contactForm = this.privatePersonForm;
        break;
    }

  }

  saveContact() {

    Object.keys(this.contactForm.controls).forEach(field => { 
      const control = this.contactForm.get(field);    
      control.markAsTouched({ onlySelf: true }); 
    });

    if (this.contactForm.valid) {

      if (this.contactPerson) {
        // update
        this.contactService.updateContact(this.contactForm.value).subscribe(data => {
          this.checkResponseData(data);
        });
      } else {
        // save
        this.contactService.saveContact(this.contactForm.value).subscribe(data => {
          this.checkResponseData(data);
        });
      }

    } else {
      this.snackBar.open("Form is not valid", 'OK');
    }
    
  }

  checkResponseData(data: any) {
    if (data) {
      if (data.success) {
        this.dialogRef.close();
        this.snackBar.open('Контакт сохранен', 'OK');
        this.contactInterService.createContact("Created successfully");
      } else {
        let errors: string = "";
        data.errors.forEach(element => {
          errors = errors.concat(element + ". ");
        });
        this.snackBar.open(errors, 'OK');
      }
    }
  }

  deleteAddress(contactPerson: ContactPerson, address: Address) {
    this.contactService.deleteContactAddress(address, contactPerson.Ref).subscribe(data =>{
        let index = contactPerson.Addresses.DoorsAddresses.indexOf(address);
        contactPerson.Addresses.DoorsAddresses.splice(index, 1);
    })
  }

}

 


