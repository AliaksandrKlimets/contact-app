import { ContactPerson } from './../core/models/contact-person.model';
import { ContactInterService } from './contact-inter.service';
import { ContactService } from './../core/services/contact.service';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator} from '@angular/material';
import { ContactModalComponent } from './contact-modal/contact-modal.component';
import { CounterpartyType } from '../core/models/counter-party-type.model';


export interface Contact {
  AdditionalPhone: string;
  Addresses: {
    DoorsAddresses: Address[], 
    WarehouseAddresses: Address[] 
  };
  CounterpartyDesctiption: string;
  CounterpartyRef: string;
  Description: string;
  Email: string;
  FirstName: string;
  Info: string;
  LastName: string;
  MarketplacePartnerDescription: string;
  MiddleName: string;
  Phones: string;
  Ref: string;
  CounterpartyType?: string;
}

export interface Address {
  Ref: string;
  SettlementRef: string;
  SettlementDescription: string;
  RegionDescription: string;
  BuildingNumber: string;
  Note: string;
}

export interface Response {
  data: any[];
  errorCodes: any[];
  errors: any[];
  info: any;
  infoCodes: any[];
  messageCodes: any[];
  success: boolean;
  warningCodes: any[];
  warnings: any[];
}


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  displayedColumns: string[] = [
    'select', 
    'CounterpartyDesctiption', 
    'FirstName', 
    'Phones', 
    'Email', 
    'Description',
    'star'
  ];
  dataSource: MatTableDataSource<Contact>;
  selection = new SelectionModel<Contact>(true, []);

  contacts: Contact[];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private contactInterService: ContactInterService,
    private contactService: ContactService) {
      this.contacts = [];
  }
  
  ngOnInit() {
    this.updateTable();
  }

  updateTable() {
    this.contactService.getAllContacts().subscribe(data => { 
      if (data) {
        this.contacts = data.data;
        this.dataSource = new MatTableDataSource<Contact>(this.contacts);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  onActionClick($event) {
    const dialogRef = this.dialog.open(ContactModalComponent);
  }

  editContact(contact: Contact) {

    let contactPerson: ContactPerson = {
      FirstName: contact.FirstName,
      MiddleName: contact.MiddleName,
      LastName: contact.LastName,
      Phone: contact.Phones,
      Email: contact.Email,
      Description: contact.Description,
      CounterpartyType: CounterpartyType.PrivatePerson,
      CounterpartyProperty: "Recipient",
      CounterpartyRef: "bb0e8da6-9a49-11e8-8b24-005056881c6b",
      Ref: contact.Ref,
      Addresses: contact.Addresses
    };

    const dialogRef = this.dialog.open(
      ContactModalComponent, {
        data: {
          contactPerson: contactPerson
        }
      }
    );
  }

  openDialog(): void {

    const dialogRef = this.dialog.open(ContactModalComponent);

    this.contactInterService.contactCreated$.subscribe(msg => {
      this.updateTable();
    });

  }

   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.dataSource) {
      this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Contact): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  }

}




