import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';
 
@Injectable()
export class ContactInterService {
 
  private contactCreatedSource = new Subject<any>();

  contactCreated$ = this.contactCreatedSource.asObservable();

  createContact(msg: any) {
    this.contactCreatedSource.next(msg);
  }

}