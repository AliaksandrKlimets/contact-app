import { ContactInterService } from './contact-inter.service';
import { ContactModalComponent } from './contact-modal/contact-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';
import { ContactRoutingModule } from './contact-routing.module';
import { MatTableModule, MatPaginatorModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    ContactRoutingModule
  ],
  declarations: [
    ContactComponent
  ],
  providers: [ContactInterService]
})
export class ContactModule { }
