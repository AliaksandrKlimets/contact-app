import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialog, MatDialogModule, MatFormFieldModule, MatCheckbox, MatCheckboxModule, MatRadioButton, MatRadioGroup, MatLabel, MatInputModule, MatRippleModule, MatButton, MatIcon, MatTableModule, MatPaginatorModule } from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { ContactModalComponent } from './contact/contact-modal/contact-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    ContactModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
  ],
  entryComponents: [ContactModalComponent],
  providers: [MatDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
