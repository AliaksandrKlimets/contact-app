import { ApiObject } from './../models/api-object.model';
import { ContactPerson } from './../models/contact-person.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Address } from 'src/app/contact/contact.component';

@Injectable()
export class ContactService {

    constructor(private http: HttpClient) { }

    private apiUrl: string = 'https://api.novaposhta.ua/v2.0/json/';

    getContactInfo(json) {
        return this.http.post<any>(this.apiUrl, json);
    }

    getAllContacts() {
        return this.http.post<any>(this.apiUrl, GET_ALL_CONTACTS);
    }

    saveContact(contactPerson: ContactPerson) {

        let apiObj: ApiObject = new ApiObject(
            "beb35606a4a40bbcee05b3e118a54bc2",
            "Counterparty",
            "save",
            contactPerson
        );

        return this.http.post<any>(this.apiUrl, apiObj);
    }

    updateContact(contactPerson: ContactPerson) {

        let apiObj: ApiObject = new ApiObject(
            "beb35606a4a40bbcee05b3e118a54bc2",
            "ContactPerson",
            "update",
            contactPerson
        );

        return this.http.post<any>(this.apiUrl, apiObj);
    }

    deleteContactAddress(address: Address, ref: String) {

        let apiObj: ApiObject = new ApiObject(
            "beb35606a4a40bbcee05b3e118a54bc2",
            "AddressContactPersonGeneral",
            "delete",
            {
                "Ref": address.Ref,
                "ContactPersonRef" : ref
            }
        );

        return this.http.post<any>(this.apiUrl, apiObj);
    }

}

const GET_ALL_CONTACTS: ApiObject = {
    "apiKey": "beb35606a4a40bbcee05b3e118a54bc2",
    "modelName": "ContactPersonGeneral",
    "calledMethod": "getContactPersonsList",
    "methodProperties": {
        "Page": 1,
        "Limit": 1000,
        "ContactProperty": "Recipient",
        "getContactPersonAddress": 1
    }
};



