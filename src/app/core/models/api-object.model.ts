export class ApiObject {
    apiKey: string;
    modelName: string;
    calledMethod: string;
    methodProperties: any;

    constructor (
        apiKey?: string,
        modelName?: string,
        calledMethod?: string,
        methodProperties?: any

    ) {
        this.apiKey = apiKey;
        this.modelName = modelName;
        this.calledMethod = calledMethod;
        this.methodProperties = methodProperties;
    }

}