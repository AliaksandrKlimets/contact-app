export enum CounterpartyType {
    PrivatePerson = "PrivatePerson",
    Organization = "Organization"
}