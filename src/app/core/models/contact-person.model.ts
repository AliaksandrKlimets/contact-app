import { CounterpartyType } from './counter-party-type.model';
import { Address } from 'src/app/contact/contact.component';

export interface ContactPerson {
  FirstName: string;
  MiddleName: string;
  LastName: string;
  Phone: string;
  Email: string;
  CityRef?: string;
  CounterpartyType: CounterpartyType,
  CounterpartyProperty: "Recipient",


  Description: string;
  CounterpartyRef?: string;
  Ref?: string;
  Addresses: {
    DoorsAddresses: Address[], 
    WarehouseAddresses: Address[] 
  };
  }
