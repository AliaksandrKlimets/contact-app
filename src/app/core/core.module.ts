import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactService } from './services';

@NgModule({
  imports: [
    CommonModule
  ],
  providers :[ContactService],
  declarations: []
})
export class CoreModule { }
